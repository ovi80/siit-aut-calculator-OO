import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class CalculatorTest {


    public Calculator setCalc(){

        Calculator calc = new Calculator();
        return calc;

    }

    @Test
    public void testCalculateSum()
    {

        Double  result = setCalc().compute(9,2,"+");
        Assert.assertEquals(11,result,0);
    }

    @Test
    public void testCalculateSumOneArgumentIsZero()
    {

        Double  result = setCalc().compute(9,0,"+");
        Assert.assertEquals(9,result,0);
    }

    @Test
    public void testCalculateSumOneNegative()
    {

        Double  result = setCalc().compute(9,-8,"+");
        Assert.assertEquals(1,result,0);
    }

    @Test
    public void testCalculateSumTwoNegative()
    {

        Double  result = setCalc().compute(-9,-2,"+");
        Assert.assertEquals(-11,result,0);
    }



    @Test
    public void testCalculateDif()
    {

        Double  result = setCalc().compute(9,2,"-");
        Assert.assertEquals(7,result,0);
    }

    @Test
    public void testCalculateDifOneNegative()
    {

        Double  result = setCalc().compute(-9,2,"-");
        Assert.assertEquals(-11,result,0);
    }

    @Test
    public void testCalculateDifTwoNegative()
    {

        Double  result = setCalc().compute(-9,-2,"-");
        Assert.assertEquals(-7,result,0);
    }

    @Test
    public void testCalculateDifFirstNumberIsZero()
    {

        Double  result = setCalc().compute(9,0,"-");
        Assert.assertEquals(9,result,0);
    }

    @Test
    public void testCalculateDifSecondNumberIsZero()
    {

        Double  result = setCalc().compute(0,9,"-");
        Assert.assertEquals(-9,result,0);
    }


    @Test
    public void testCalculateMult()
    {

        Double  result = setCalc().compute(9,2,"*");
        Assert.assertEquals(18,result,0);
    }

    @Test
    public void testCalculateMultOneNegative()
    {

        Double  result = setCalc().compute(-9,2,"*");
        Assert.assertEquals(-18,result,0);
    }

    @Test
    public void testCalculateMultTwoNegative()
    {

        Double  result = setCalc().compute(-9,-2,"*");
        Assert.assertEquals(18,result,0);
    }

    @Test
    public void testCalculateMultOneNumberIsZero()
    {

        Double  result = setCalc().compute(9,0,"*");
        Assert.assertEquals(0,result,0);
    }

    @Test
    public void testCalculateDiv()
    {

        Double  result = setCalc().compute(9,3,"/");
        Assert.assertEquals(3,result,0);
    }

    @Test
    public void testCalculateDivOneNegative()
    {

        Double  result = setCalc().compute(-9,3,"/");
        Assert.assertEquals(-3,result,0);
    }

    @Test
    public void testCalculateDivTwoNegative()
    {

        Double  result = setCalc().compute(-9,-3,"/");
        Assert.assertEquals(3,result,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivByZero()
    {

        Double  result = setCalc().compute(9,0,"/");
    }

    @Test
    public void testCalculateDivFirstOperatorIsZero()
    {

        Double  result = setCalc().compute(0,3,"/");
        Assert.assertEquals(0,result,0);
    }

    @Test
    public void testCalculateSQRT()
    {

        Double  result = setCalc().compute(9,3,"SQRT");
        Assert.assertEquals(3,result,0);
    }

    @Test(expected = NumberFormatException.class)
    public void testSQRTNegative()
    {

        Double  result = setCalc().compute(-9,0,"SQRT");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongOperator()
    {

        Double  result = setCalc().compute(7,0,"a");
    }




	
}
